(*
** cgi_functions.ml for OCaml-WebFramework
** 
** Copyright (C) 2012 Pierre Surply
** <pierre.surply@gmail.com>
**
** This file is part of OCaml-WebFramework.
**
**    OCaml-WebFramework is free software: you can redistribute it and/or modify
**    it under the terms of the GNU General Public License as published by
**    the Free Software Foundation, either version 3 of the License, or
**    (at your option) any later version.
**
**    OCaml-WebFramework is distributed in the hope that it will be useful,
**    but WITHOUT ANY WARRANTY; without even the implied warranty of
**    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**    GNU General Public License for more details.
**
**    You should have received a copy of the GNU General Public License
**    along with OCaml-WebFramework.  If not, see <http://www.gnu.org/licenses/>.
** 
** Started on  Sat Sep 15 21:11:04 2012 Pierre Surply
Last update Thu Oct 11 15:34:58 2012 pierre surply
*)

let get_function = function
  | "get_time" -> Date.get_date ()
  | f -> "Cannot find function : " ^ f

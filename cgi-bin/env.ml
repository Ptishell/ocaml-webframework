(*
** env.ml for OCaml-WebFramework
** 
** Copyright (C) 2012 Pierre Surply
** <pierre.surply@gmail.com>
**
** This file is part of OCaml-WebFramework.
**
**    OCaml-WebFramework is free software: you can redistribute it and/or modify
**    it under the terms of the GNU General Public License as published by
**    the Free Software Foundation, either version 3 of the License, or
**    (at your option) any later version.
**
**    OCaml-WebFramework is distributed in the hope that it will be useful,
**    but WITHOUT ANY WARRANTY; without even the implied warranty of
**    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**    GNU General Public License for more details.
**
**    You should have received a copy of the GNU General Public License
**    along with OCaml-WebFramework.  If not, see <http://www.gnu.org/licenses/>.
** 
** Started on  Sun Sep 16 13:57:47 2012 Pierre Surply
Last update Thu Oct 11 15:34:01 2012 pierre surply
*)

let get_env str =
  try
    Sys.getenv str
  with Not_found -> ""

let url =
  let req_method = get_env "REQUEST_METHOD" in
  match req_method with
    | "GET" | "HEAD" -> get_env "QUERY_STRING"
    | _ -> ""

let url_arg =
  let regexsl = Str.regexp "%2F" in
  let url = Str.global_replace regexsl "/" url in
  let regex = Str.regexp "\\([^/]*\\)/" in
  let rec build_url_arg s start =
    if (try Str.search_forward regex s start
      with Not_found -> -1) > -1 then
      begin
	let start = Str.match_end () in
	let arg = Str.matched_group 1 s in
	arg::build_url_arg s start
      end
    else []
  in
  build_url_arg url 0

let get_url_arg n =
  let rec r_get n = function
    | [] -> ""
    | h::t ->
      if n = 0 then h
      else r_get (n-1) t
  in
  r_get n url_arg

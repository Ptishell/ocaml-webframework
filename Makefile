##
## Makefile for OCaml-WebFramework
## 
## Copyright (C) 2012 Pierre Surply
## <pierre.surply@gmail.com>
##
## This file is part of OCaml-WebFramework.
##
##    OCaml-WebFramework is free software: you can redistribute it and/or modify
##    it under the terms of the GNU General Public License as published by
##    the Free Software Foundation, either version 3 of the License, or
##    (at your option) any later version.
##
##    OCaml-WebFramework is distributed in the hope that it will be useful,
##    but WITHOUT ANY WARRANTY; without even the implied warranty of
##    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
##    GNU General Public License for more details.
##
##    You should have received a copy of the GNU General Public License
##    along with OCaml-WebFramework.  If not, see <http://www.gnu.org/licenses/>.
## 
## Started on  Fri Sep 14 20:30:24 2012 Pierre Surply
## Last update Sun Feb 10 14:35:21 2013 Pierre Surply
##

EXEC = ocaml-webframework.cgi
export EXEC

all: cgi-bin

clean: COMMAND=clean
clean: cgi-bin

test: COMMAND=test
test: cgi-bin

cgi-bin::
	@$(MAKE) -C $@ $(COMMAND)
